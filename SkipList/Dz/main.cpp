#include "../SkipList/std.hpp"
#include <fstream>

using namespace std;

int main( int argc, char* argv[] )
{
	string inPath, outPath;
	try
	{
		inPath = argv[ 1 ];
		outPath = argv[ 2 ];
	}
	catch ( ... )
	{
		cout << "wrong arguments ><" << endl;
		return -1;
	}

	ifstream input;
	input.open( inPath );

	ofstream output;
	output.open( outPath );

	SkipList list( 0.5 );
	while ( input )
	{
		int key;
		string command, value;
		input >> command;
		if (command == "add")
		{
			input >> key >> value;
			output << "Adding element to SkipList: {key: " << key << ", value: " << value << "}" << endl;
			list.add( key, value );
			output << list.printToString() << endl;
		} 
		else if ( command == "remove" )
		{
			input >> key;
			output << "Removing element of SkipList: {key: " << key << "}" << endl;
			list.remove( key );
			output << list.printToString( ) << endl;
		}
		else if ( command == "get" )
		{
			input >> key;
			output << "Element with {key: " << key << "}: " << list.get( key ) << endl << endl;
		}
		else if ( command == "min" )
		{
			output << "Min element of SkipList: " << list.getMin( ) << endl << endl;
		}
		else if ( command == "max" )
		{
			output << "Max element of SkipList: " << list.getMax( ) << endl << endl;
		} 
	}
}