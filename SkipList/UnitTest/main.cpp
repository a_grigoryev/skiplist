#include "../SkipList/std.hpp"

int main()
{

	SkipList l( 0.5 );
	l.add( 1, "one" );
	l.print();
	l.add( 5, "five" );
	l.print();
	l.add( 124, "dohua" );
	l.print();
	l.add( 3, "three" );
	l.print();
	l.add( 43, "forty_three" );
	l.print();
	l.add( 56, "fifty_six" );
	l.print();
	l.add( 2, "two" );
	l.print();
	l.add( 1, "one_again" );

	std::cout << l.get( 43 ) << std::endl;
	std::cout << l.get( 5 ) << std::endl;
	std::cout << l.get( 3 ) << std::endl;
	std::cout << l.get( 2 ) << std::endl;
	std::cout << l.get( 2 ) << std::endl;

	std::cout << std::endl;

	std::cout << "min: " << l.getMin() << std::endl;
	std::cout << "max: " << l.getMax( ) << std::endl;
	
	std::cout << std::endl;

	l.remove( 43 );
	l.print();
	l.remove( 5 );
	l.print();
	l.remove( 2 );
	l.print();
	l.remove( 124 );
	l.print();

}