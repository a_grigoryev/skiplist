#include "std.hpp"

using namespace std;

SkipListNode::SkipListNode( int key_, string value_, size_t height_ )
	: _key( key_ )
	, _value( value_)
	, height( height_ )
	, next( height_ )
{
}

SkipListNode::SkipListNode()
	: height( 1 )
{
	addLevel();
}

SkipListNode::~SkipListNode()
{
}

int SkipListNode::key()
{
	return _key;
}

string SkipListNode::value()
{
	return _value;
}
void SkipListNode::setValue(string value)
{
	_value = value;
}

size_t SkipListNode::getHeight()
{
	return height;
}

void SkipListNode::addLevel()
{
	next.push_back( nullptr );
	height++;
}

NodePtr SkipListNode::nextOnLevel( size_t level )
{
	if (level < height)
	{
		return next[ level ];
	}
	else
	{
		return nullptr;
	}
}

NodePtr SkipListNode::nextOnHeight( size_t height )
{
	return nextOnLevel( height - 1 );
}

void SkipListNode::setNext( size_t level, NodePtr newNext )
{
	if ( level < height )
	{
		next[ level ] = newNext;
	}
}
