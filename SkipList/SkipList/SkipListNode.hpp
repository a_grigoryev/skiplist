#pragma once

class SkipListNode
{
public:
	SkipListNode(int _key, std::string _value, size_t _height);
	SkipListNode();
	~SkipListNode();
	
	NodePtr nextOnLevel( size_t level );
	NodePtr nextOnHeight( size_t height );
	void setNext( size_t level, NodePtr newNext );
	
	int key();
	std::string value();
	void setValue(std::string value);
	size_t getHeight( );

	//for using in head node only
	void addLevel(); 

private:
	int _key;
	std::string _value;
	std::vector<NodePtr> next;
	size_t height;
};
