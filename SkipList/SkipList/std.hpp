//precompiled header

#include <string>
#include <memory>
#include <vector>
#include <ctime>
#include <cstdlib>

#include <iostream>
#include <iomanip>
#include <sstream>

class SkipListNode;		
typedef std::shared_ptr<SkipListNode> NodePtr;

#include "SkipListNode.hpp"
#include "SkipList.hpp"
