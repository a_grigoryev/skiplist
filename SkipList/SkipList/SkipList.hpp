#pragma once

class SkipList
{
public:

	struct FindResponse
	{
		std::vector<NodePtr> nodesToUpdate;
		NodePtr element;
	};

	SkipList( float newLevelProbability);
	~SkipList();

	std::string get( int key );
	void add( int key, std::string value );
	void remove( int key );

	std::string getMax();
	std::string getMin();

	void print( );
	std::string printToString();

private:
	size_t randomHeight();
	FindResponse findElement( int keyForSearch );
	
	float newLevelProbability;
	size_t listHeight; 
	size_t length;
	NodePtr head;
	NodePtr tail;
};

