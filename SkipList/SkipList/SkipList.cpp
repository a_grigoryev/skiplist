#include "std.hpp"

using namespace std;

SkipList::SkipList(float _newLevelProbability)
	: newLevelProbability( _newLevelProbability )
	, listHeight( 1 )
	, length( 0 )
{
	head = make_shared<SkipListNode>();
	srand( time( NULL ) );
}

SkipList::~SkipList()
{
}

string SkipList::get( int keyForSearch )
{
	FindResponse findResponse = findElement(keyForSearch);
	NodePtr next = findResponse.element->nextOnLevel(0);
	
	if ( next && next->key() == keyForSearch )
	{
		return next->value();
	}
	else
	{
		return string();
	}
}

void SkipList::add( int inputKey, string inputValue )
{
	FindResponse findResponse = findElement( inputKey );
	NodePtr next = findResponse.element->nextOnLevel(0);
	vector<NodePtr> nodesToUpdate = findResponse.nodesToUpdate;

	if ( next && next->key() == inputKey )
	{
		next->setValue( inputValue );
		return;
	}
		 
	// generate randomized height for new node
	size_t height = randomHeight();

	// insert new element after current
	if (height > listHeight)
	{
		++listHeight;
		nodesToUpdate[ height - 1 ] = head;
		head->addLevel();
	}
	NodePtr newNode = make_shared<SkipListNode>( inputKey, inputValue, height );
	for ( size_t currentLevel = 0; currentLevel < height; currentLevel++ )
	{
		newNode->setNext( currentLevel, nodesToUpdate[ currentLevel ]->nextOnLevel( currentLevel ) );
		nodesToUpdate[ currentLevel ]->setNext( currentLevel, newNode );
	}
	length++;
}

void SkipList::remove( int deleteKey )
{
	FindResponse findResponse = findElement( deleteKey );
	NodePtr next = findResponse.element->nextOnLevel(0);
	vector<NodePtr> nodesToUpdate = findResponse.nodesToUpdate;

	if ( next && next->key( ) == deleteKey )
	{
		for ( size_t currentLevel = 0; currentLevel < next->getHeight(); ++currentLevel )
		{
			nodesToUpdate[ currentLevel ]->setNext( currentLevel, next->nextOnLevel( currentLevel ) );
		}
		// reduce list height if necessary 
		while ( !head->nextOnLevel( listHeight - 1 ) && listHeight > 1 )
		{
			--listHeight;
		}
		--length;
	}
}

string SkipList::getMax()
{
	NodePtr current = head;
	while ( current->nextOnLevel( 0 ) )
	{
		current = current->nextOnLevel( 0 );
	}
	return current == head ? string() : current->value();
}

string SkipList::getMin()
{
	NodePtr first = head->nextOnLevel( 0 );
	return first ? first->value() : string() ;
}

size_t SkipList::randomHeight()
{
	size_t height = 1;
	while ( (double)rand() / RAND_MAX  < newLevelProbability && height < listHeight + 1 )
	{
		++height;
	}
	return height;
}

SkipList::FindResponse SkipList::findElement(int keyForSearch)
{
	FindResponse response;
	vector<NodePtr> nodesToUpdate( listHeight + 1 );

	NodePtr current = head;
	NodePtr next;
	int currentLevel;
	for ( currentLevel = listHeight - 1; currentLevel >= 0; --currentLevel )
	{
		next = current->nextOnLevel( currentLevel );
		while ( next && next->key() < keyForSearch )
		{
			current = next;
			next = next->nextOnLevel( currentLevel );
		}
		nodesToUpdate[ currentLevel ] = current;
	}
	response.element = current;
	response.nodesToUpdate = nodesToUpdate;
	
	return response;
}

string SkipList::printToString()
{
	string report;
	ostringstream reportStream( report );
	vector<vector<string>> matrix( listHeight );

	for ( size_t i = 0; i < listHeight; i++ )
	{
		matrix[ i ].resize( length );
		for ( size_t j = 0; j < length; j++ )
		{
			matrix[ i ][ j ] = "---";
		}
	}

	int currentLevel;
	NodePtr current = head->nextOnLevel( 0 );
	size_t currentOffset = 0;
	while ( current )
	{
		for ( currentLevel = 0; currentLevel < current->getHeight(); ++currentLevel )
		{
			matrix[ currentLevel ][ currentOffset ] = to_string( current->key() );
		}
		current = current->nextOnLevel( 0 );
		currentOffset++;
	}
	reportStream << endl << "SkipList [ " << "length: " << length << ", height: " << listHeight << " ]:" << endl;
	for ( size_t i = 0; i < listHeight; i++ )
	{
		reportStream << "|";
		for ( size_t j = 0; j < length; j++ )
		{
			reportStream << setw( 3 ) << matrix[ i ][ j ] << "--->";
		}
		reportStream << endl;
	}
	return reportStream.str();
}

void SkipList::print()
{
	cout << printToString() << endl;
}